//
//  ExpandingCell.swift
//  BitcoinInvest
//
//  Created by Maxim Shmotin on 03/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

class ExpandingCell: UITableViewCell {
    @IBOutlet var addressNameLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var balanceLabel: UILabel!
    @IBOutlet var deltaProfitLabel: UILabel!
    @IBOutlet var lastTransactionTv: UITextView!
    @IBOutlet var moreButton: UIButton!
    weak var vc: UIViewController!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func more(sender: UIButton){
        vc.performSegueWithIdentifier("Transaction", sender: self)
    }

}
