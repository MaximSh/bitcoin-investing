//
//  AddressInfo.swift
//  BitcoinInvest
//
//  Created by Maxim Shmotin on 31/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

public class AddressInfo: NSObject, NSCoding {
    private let OBJECT_ADDRESSINFO_KEY: NSString = "addressInfo"
    private let NUMBER_OF_SIGNS: Double = 100000000.0
    
    public var label: String?
    public var finalBalance: Double
    private var numberTransaction: Int
    private var unredeemedTrans: Int
    public var transactions:[Transaction]!
    
    override init(){
        self.label = "UnknownSource"
        self.finalBalance = 0.0
        self.numberTransaction = 0
        self.unredeemedTrans = 0
        self.transactions = []
        super.init()
    }
    
    init(finalBalance: Double, numberTransaction: Int, unredeemedTrans: Int, transactions: [Transaction] ){
        self.finalBalance = finalBalance/NUMBER_OF_SIGNS
        self.numberTransaction = numberTransaction
        self.unredeemedTrans = unredeemedTrans
        self.transactions = transactions
        super.init()
    }
    
    required public init(coder: NSCoder){
        self.finalBalance = coder.decodeDoubleForKey("finalBalance")
        self.numberTransaction = coder.decodeIntegerForKey("numberTransaction")
        self.unredeemedTrans = coder.decodeIntegerForKey("unredeemedTrans")
        self.transactions = coder.decodeObjectForKey("transactions") as [Transaction]
        super.init()
    }
    
    public func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeDouble(self.finalBalance, forKey: "finalBalance")
        aCoder.encodeInteger(self.numberTransaction, forKey: "numberTransaction")
        aCoder.encodeInteger(self.unredeemedTrans, forKey: "unredeemedTrans")
        aCoder.encodeObject(self.transactions, forKey: "transactions")
    }
}

