//
//  Extension+Double.swift
//  BitcoinInvest
//
//  Created by Maxim Shmotin on 31/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

extension Double {
    func format() -> String {
        let decimalPlaces: String = ".3"
        return NSString(format: "%\(decimalPlaces)f", self)
    }
}
