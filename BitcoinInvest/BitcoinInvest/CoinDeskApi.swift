//
//  CoinDeskApi.swift
//  BitcoinInvest
//
//  Created by Maxim Shmotin on 20/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

public class CoinDeskApi: ApiService {
    private let API_START_DATE_PRICE = "http://api.coindesk.com/v1/bpi/historical/close.json?start=" //2013-09-01
    private let API_END_DATE_PRICE = "&end="
    
    override init () {
        super.init()
    }
    /**
        Get BPI in USD by a certain date
        @param date format should be, for example, 2013-09-01
    */
    public func getBPIForDate(date: String) -> NSData {
        let request = "\(API_START_DATE_PRICE)\(date)\(API_END_DATE_PRICE)\(date)"
        return getResponseFromPublicServerUrl(request)
    }
    
}
