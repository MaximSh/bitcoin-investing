//
//  Transaction.swift
//  BitcoinInvest
//
//  Created by Maxim Shmotin on 02/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//


public class Transaction: NSObject, NSCoding {
    private let NUMBER_OF_SIGNS: Double = 100000000.0
    public let fromAddress = [String]()
    public let toAddress = [String]()
    public let amount: Double = 0.0
    public let time: Double = 0.0
    
    public var fullInfo: String{
        get{
            return "\(fromAddress) - \(fromAddress): \(amount): \(time)"
        }
    }
    
    init(fromAddress: [String], toAddress: [String], amount: Double){
        self.fromAddress = fromAddress
        self.toAddress = toAddress
        self.amount = amount/NUMBER_OF_SIGNS
        super.init()
    }
    
    required public init(coder: NSCoder){
        super.init()
        self.fromAddress = coder.decodeObjectForKey("bFromAddress") as [String]
        self.toAddress = coder.decodeObjectForKey("bToAddress") as [String]
        self.amount = coder.decodeDoubleForKey("bAmount")
        self.time  = coder.decodeDoubleForKey("bTransaction")
    }
    
    public func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(time, forKey: "bFromAddress")
        aCoder.encodeObject(time, forKey: "bToAddress")
        aCoder.encodeDouble(time, forKey: "bAmount")
        aCoder.encodeDouble(time, forKey: "bTransaction")
    }
    
    func setTime(time: Double){
        self.time = time
    }

}
