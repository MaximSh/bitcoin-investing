//
//  Extension+NSData.swift
//  BitcoinInvest
//
//  Created by Maxim Shmotin on 20/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

extension NSData {
    
func getJsonFromNSData() -> NSDictionary{
    var error: NSError?
    if let json = NSJSONSerialization.JSONObjectWithData(self, options: NSJSONReadingOptions.MutableContainers, error: &error) as? NSDictionary{
        return json
    }else{
        println("Enable to do NSJSONSerialization.")
    }
    return NSDictionary()
}
}

