//
//  Extension+String.swift
//  BitcoinInvest
//
//  Created by Maxim Shmotin on 20/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

extension String{

    func extractBitcoinAddress() -> String{
        var resultString = ""
        
        resultString = self.stringByReplacingOccurrencesOfString("/", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        if contains(resultString, ":"){
            let rangeToSymbol = resultString.rangeOfString(":")
            let index: Int = distance(resultString.startIndex, rangeToSymbol!.startIndex )
            resultString = resultString.substringFromIndex(advance(resultString.startIndex, index  + 1))
        }
        
        if contains(resultString, "?"){
            let rangeToSymbol = resultString.rangeOfString("?")
            var index: Int = distance(resultString.startIndex, rangeToSymbol!.startIndex)
            resultString = resultString.substringToIndex(advance(resultString.startIndex, index))
        }
        println("Result string: \(resultString)")
        return resultString
    }
}

