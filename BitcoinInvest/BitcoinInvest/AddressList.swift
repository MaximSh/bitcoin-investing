//
//  Address.swift
//  BitcoinInvest
//
//  Created by Maxim Shmotin on 02/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

public class AddressList: NSObject, NSCoding {
    private enum UDKey: String {
        case USER_DEFAULTS_KEY = "AddressTT"
        case KEY_FOR_ADDRESSES = "myAddresses"
    }
    private var myAddresses = Dictionary<String, AddressInfo>()
    public var addresses: [String]{
        get{
            return myAddresses.keys.array
        }
    }
    
    class var sharedInstance: AddressList {
        struct Static {
            static var instance: AddressList?
        }
        if !(Static.instance != nil) {
            Static.instance = self.loadSaved()
        }
        return Static.instance!
    }
    
    
    // Address List Main Methods
    
    required public init(coder: NSCoder){
        super.init()
        if let addresses  = coder.decodeObjectForKey(UDKey.KEY_FOR_ADDRESSES.rawValue) as? [String : AddressInfo] {
            self.myAddresses = addresses
        }
    }
    
    public func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(myAddresses, forKey: UDKey.KEY_FOR_ADDRESSES.rawValue)
    }
    
    public override init(){}
    
    public func save(){
        let newDict = self.myAddresses
        let data = NSKeyedArchiver.archivedDataWithRootObject(newDict)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: UDKey.USER_DEFAULTS_KEY.rawValue) //"AddressList"
    }
    
     class func loadSaved() -> AddressList{
        let c = AddressList()
        if let data = NSUserDefaults.standardUserDefaults().objectForKey(UDKey.USER_DEFAULTS_KEY.rawValue) as? NSData{
            if let addresses = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [String : AddressInfo] {
                c.myAddresses = addresses
            }
        }
        return c
    }
    
    private func parseTransactions(txsArray: NSArray){
        //println(txsArray)
        let out: NSDictionary = txsArray[1] as NSDictionary
        println(out)
        for transaction in txsArray {
            // All inputs to out, for example 4 inp -> 1 out or 1 inp -> 2 out
            //let tnz = Transaction()
            var inputsArray = [String](), outputArray = [String]()
            
            let inputs = transaction["inputs"] as? NSArray
            let out = transaction["out"] as? NSArray
            let time = transaction["time"] as? Double

            for element in inputs! {
                let prev_out = element["prev_out"] as NSDictionary
                let addr = prev_out["addr"] as String
                let value = prev_out["value"] as Double
                inputsArray.append(addr)
            }
            
            for element in out! {
                let address  = element["addr"] as String
                let value  = element["value"] as String
                
            }
            
        }
        /*for (key, value) in txsArray as NSDictionary {
          println(key, value)
        }*/
        
    }
    
    // Address List Methods
    
    func add(json: NSDictionary, completionHandler: (String?) -> Void){
        if let id = json["address"] as? String{
            if (!contains(myAddresses.keys, id)){
                self.parseTransactions(json["txs"] as NSArray)
                
                myAddresses[id] = AddressInfo(finalBalance: json["final_balance"] as Double,
                    numberTransaction: json["n_tx"] as Int,
                    unredeemedTrans: json["total_received"] as Int,
                    transactions: json["txs"] as [Transaction]!)
                self.save()
            }else{
                completionHandler(nil)
            }
            completionHandler(id)
        }
    }
    
    func remove(key: String){
        myAddresses.removeValueForKey(key)
        self.save()
    }
    
    func getBalanceForAddress(address: String, currentRate: Double?) -> String {
        let finalBalance = myAddresses[address]?.finalBalance
        if let rate = currentRate{
            return "$\((finalBalance! * rate).format())"
        }else{
            return "Ƀ\(finalBalance!.format())"
        }
    }
    
    func getLabelForAddress(address: String) -> String? {
        return self.myAddresses[address]?.label
    }
    
    func setLabelForAddress(address: String, label text: String){
        self.myAddresses[address]?.label = text
    }
    
    func getProfitDelta(address: String) -> String {
        return ""
    }
    
    func getLastTransactionForAddress(address: String) -> Bool {
        return false
    }
    
    func getTransactionList(forAddress address: String) -> [Transaction]? {
        return myAddresses[address]?.transactions
    }
}