//
//  ApiService.swift
//  BitcoinInvest
//
//  Created by Maxim Shmotin on 20/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

public class ApiService: NSObject {
    
    override init () {
        super.init()
    }
    
    public func getResponseFromPublicServerUrl(urlString: NSString) -> NSData {
        NSLog(urlString)
        let url = NSURL(string: urlString)
        let request = NSURLRequest(URL: url!)
        var theResponse: AutoreleasingUnsafeMutablePointer <NSURLResponse?>=nil
        
        var responseData = NSURLConnection.sendSynchronousRequest(request, returningResponse: theResponse, error:nil) as NSData!
        if (responseData == nil){
            return NSData()
        }
        //println(NSString(data: responseData, encoding: NSUTF8StringEncoding)!)
        return responseData!
    }
}
