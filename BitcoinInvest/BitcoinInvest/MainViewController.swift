//
//  MainViewController.swift
//  BitcoinInvest
//
//  Created by Maxim Shmotin on 02/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource, QRCodeReaderDelegate {
    @IBOutlet var toolbar: UIToolbar!
    private var selectedIndex: Int!
    private var lastBtcUsdRate: Double?
    private var tracker: NSTimer!
    private var apiHandler: BlockchainApi = BlockchainApi()
    private var items = AddressList.sharedInstance.addresses
    private var lastRate: AnyObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tracker = NSTimer.scheduledTimerWithTimeInterval(8.0, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
        
        self.selectedIndex = -1
        
        self.tableView.registerNib(UINib(nibName: "ExpandingCell", bundle: nil), forCellReuseIdentifier: "expandingCell")
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        self.tableView.allowsMultipleSelectionDuringEditing = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Table View
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("expandingCell") as ExpandingCell
        
        cell.addressNameLabel.text = AddressList.sharedInstance.getLabelForAddress(items[indexPath.row])
        cell.addressLabel.text = items[indexPath.row]
        cell.balanceLabel.text = AddressList.sharedInstance.getBalanceForAddress(items[indexPath.row], currentRate: lastRate as? Double)
        cell.vc = self
        //cell.deltaProfitLabel.text = "\(AddressList.sharedInstance.getProfitDelta(items[indexPath.row]))"
        return cell
    }
    
    private let EXPANDING_ROW_HEIGHT: CGFloat = 140
    private let STANDART_ROW_HEIGHT: CGFloat = 60
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if (selectedIndex == indexPath.row){
            return EXPANDING_ROW_HEIGHT
        }else{
            return STANDART_ROW_HEIGHT
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (selectedIndex == indexPath.row){
            selectedIndex = -1
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            return
        }
        
        if (selectedIndex != -1){
            let prevPath = NSIndexPath(forRow: selectedIndex, inSection: 0)
            selectedIndex = indexPath.row
            tableView.reloadRowsAtIndexPaths([prevPath], withRowAnimation: UITableViewRowAnimation.Fade)
        }
        
        selectedIndex = indexPath.row
        tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)

    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete){
            AddressList.sharedInstance.remove(items[indexPath.row])
            self.items = AddressList.sharedInstance.addresses
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        }
    }
    
    // Scanner
    
    @IBAction func scanQRcode(sender: UIBarButtonItem){
        let reader = QRCodeReaderViewController()
        reader.modalPresentationStyle = UIModalPresentationStyle.FormSheet
        reader.delegate = self
        self.presentViewController(reader, animated: true, completion: nil)
    }
    
    func reader(reader: QRCodeReaderViewController!, didScanResult result: String!) {
        self.dismissViewControllerAnimated(true, completion: {
            self.getInfoForAddress(result.extractBitcoinAddress())
        })
    }
    
    func readerDidCancel(reader: QRCodeReaderViewController!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // Logic

    func update(){
        updateBTC_USDRate()
    }
    
    func updateBTC_USDRate(){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let data =  self.apiHandler.getLastBitcoinPrice()
            let json = data.getJsonFromNSData()
            dispatch_sync(dispatch_get_main_queue(), { () -> Void in
                let usdRate: AnyObject? = json["USD"]
                self.lastRate = usdRate!["last"]!
                self.navigationItem.title = "BTC/USD: $\(self.lastRate!)"
                self.tableView.reloadData()
            })
        }
    }

    func getInfoForAddress(address: String){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let data =  self.apiHandler.getDataForWalletAddress(address)
            let json = data.getJsonFromNSData()
            dispatch_async(dispatch_get_main_queue(), {
                AddressList.sharedInstance.add(json, completionHandler: { (address) -> Void in
                    if (address == nil){
                        self.itemAlreadyExistDialog()
                    }else{
                        self.itemSuccessfullyAddedDialog(address!)
                    }
                })
                self.items = AddressList.sharedInstance.addresses
                self.tableView.reloadData()
            })
        }
    }
        
    func itemAlreadyExistDialog(){
        let dialog = UIAlertController(title: "Warning", message: "Address already exist", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel){(action) in}
        dialog.addAction(cancelAction)
        self.presentViewController(dialog, animated: true, completion: nil)
    }
    
    func itemSuccessfullyAddedDialog(address: String){
        let dialog = UIAlertController(title: "Added!", message: "Address \(address) successfully added!", preferredStyle: UIAlertControllerStyle.Alert)
        dialog.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "Bitcoin Source"
        }
        let acceptAction = UIAlertAction(title: "Accept", style: .Cancel) { (action) in
            let label = dialog.textFields![0] as UITextField
            AddressList.sharedInstance.setLabelForAddress(address, label: label.text)
        }
        dialog.addAction(acceptAction)
        self.presentViewController(dialog, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "Transaction"){
            let transactionVC = segue.destinationViewController as TransactionViewController
            transactionVC.transactions = AddressList.sharedInstance.getTransactionList(forAddress: items[selectedIndex])
        }
    }
    
}

